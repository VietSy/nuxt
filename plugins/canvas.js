var size,
	circleM,
	scaleRatio,
	ccM,
	circle1,
	circle2,
	circle3,
	x,
	midX,
	midY,
	firstMidX,
	firstMidY,

	bubble,
	Tau = Math.PI * 2,
	debug = false,
	sizeCircle = 400,
	Wcircle = sizeCircle,
	Hcircle = sizeCircle;

function circleMouse(){	 
	circleM = document.getElementById("js-pointer1");

	scaleRatio = window.devicePixelRatio;

	circleM.width = Wcircle * scaleRatio;
	circleM.height = Hcircle * scaleRatio;
	circleM.style.width = Wcircle + "px";
	circleM.style.height = Hcircle + "px";
	circleM.style.left = "50%";
	circleM.style.top = "50%";
	circleM.style.marginLeft = -(circleM.width / 2) / scaleRatio + "px";
	circleM.style.marginTop = -(circleM.height / 2) / scaleRatio + "px";

	ccM = circleM.getContext("2d");
	ccM.scale(scaleRatio, scaleRatio);

	circle1 = new circle(Wcircle/2, Hcircle/2, Wcircle/3, 9, "#4e6e45");

	circle2 = new circle(Wcircle/2, Hcircle/2, Wcircle/3, 9, "#4e6e45");
	circle3 = new circle(Wcircle/2, Hcircle/2, Wcircle/3, 9, "#4e6e45");

	window.requestAnimationFrame(update);
}

function update(timestamp) {
	ccM.save();
	ccM.clearRect(0, 0, Wcircle, Hcircle);

	circle1.draw();
	circle2.draw();
	circle3.draw();
	ccM.restore();
	window.requestAnimationFrame(update);
}

function circle(centerX, centerY, radius, numberOfPoints, color){
	this.centerX = centerX;
	this.centerY = centerY;
	this.radius = radius;
	this.numberOfPoints = numberOfPoints;
	this.color = color;
	this.angleIncrement = Math.PI * 2 / this.numberOfPoints;
	this.points = [];

	for (var i = 0; i < this.numberOfPoints; i++){
		this.points.push(new Point(this.centerX, this.centerY, this.angleIncrement * i, this.radius));
	};

	this.draw = function(){
		for (var i = 0; i < this.points.length; i++){
			this.points[i].draw();
		};

		ccM.strokeStyle = this.color;
		ccM.beginPath();
		for (var i =0; i<this.points.length - 1; i++){
			ccM.moveTo(this.points[i].x, this.points[i].y);
			ccM.lineTo(this.points[i+1].x, this.points[i+1].y);

		};
		ccM.moveTo(this.points[this.points.length - 1].x, this.points[this.points.length - 1].y);
		ccM.lineTo(this.points[0].x, this.points[0].y);

		ccM.stroke();

		ccM.strokeStyle = this.color;
		ccM.beginPath();

		for(var i = 0; i < this.points.length; i++) {
			if(i < this.points.length - 1) {
				midX = (this.points[i].x + this.points[i+1].x)/2;
				midY = (this.points[i].y + this.points[i+1].y)/2;

			} else {
				midX = (this.points[i].x + this.points[0].x)/2;
				midY = (this.points[i].y + this.points[0].y)/2;
			}

			if(i==0){
				ccM.moveTo(midX,midY);
				firstMidX = midX;
				firstMidY = midY;
			} else {
				ccM.quadraticCurveTo(this.points[i].x,this.points[i].y, midX, midY);
			}
		}

		ccM.quadraticCurveTo(this.points[0].x,this.points[0].y, firstMidX, firstMidY);
		ccM.stroke();
	};

	return this;
};

function Point(centerX, centerY, angle, radius){
	this.centerX = centerX;
	this.centerY = centerY;
	this.angle = angle;
	this.radius = radius;

	this.amplitude = 8 + Math.random() * 8;

	this.speed = 0.01 + Math.random() * 0.05;
	this.t = Math.random() * Math.PI * 2;

	this.draw = function(){
		this.t += this.speed;
		this.sway = this.amplitude * Math.cos(this.t);


		this.x = this.centerX + Math.cos(this.angle) * (this.radius + this.sway);
		this.y = this.centerY + Math.sin(this.angle) * (this.radius + this.sway);
		ccM.fillStyle = "#4e6e45";
		ccM.beginPath();
		ccM.arc(this.x, this.y, 2, 0, Math.PI * 2);
		ccM.fill();
	};

	return this;
};

circleMouse();

/*------------------------------------------------------------

------------------------------------------------------------*/
var rainbowM,
	rbM,
	sizeRainbox = 200,
	Wrainbox = sizeRainbox,
	Hrainbox = sizeRainbox,
	opts = {
		lineMaxCount: 40,
		lineSpawnProb: .1,
		lineMaxLength: 10,
		lineIncrementProb: .5,
		lineDecrementProb: .7,
		lineSafeTime: 150,
		lineMidJitter: 7,
		lineMidPoints: 3,
		lineHueVariation: 30,
		lineAlpha: 1,

		gridSideNum: 6,
		gridSide: 30,
		gridRotationVel: .002,
		gridScalingInputMultiplier: .01,
		gridScalingMultiplier: .3,
		gridHueChange: .6,
		gridRepaintAlpha: .1,
		gridCenterX: Wrainbox/2,
		gridCenterY: Hrainbox/2
	},

	tick = ( Math.random() * 360 ),
	lines = [],
	linePart, s,
	radPart = Math.PI * 2 / opts.gridSideNum;

function rainbowMouse() {

	rainbowM = document.getElementById("js-pointer2");

	rainbowM.width = Wrainbox * scaleRatio;
	rainbowM.height = Hrainbox * scaleRatio;
	rainbowM.style.width = Wrainbox + "px";
	rainbowM.style.height = Hrainbox + "px";
	rainbowM.style.left = "50%";
	rainbowM.style.top = "50%";
	rainbowM.style.marginLeft = -(rainbowM.width / 2) / scaleRatio + "px";
	rainbowM.style.marginTop = -(rainbowM.height / 2) / scaleRatio + "px";

	rbM = rainbowM.getContext( '2d' );

	window.requestAnimationFrame( rainbowMouse );

	rainbowMouse.step();
	rainbowMouse.draw();
}
rainbowMouse.step = function() {

	rainbowMouse.step.spawn();
	rainbowMouse.step.updateTick();

	lines.map( function( line ) { line.step(); } );
}
rainbowMouse.draw = function() {

	linePart = 1 / opts.lineMidPoints;
	s = opts.gridSide;

	rainbowMouse.draw.repaint();
	rainbowMouse.draw.transform();

	lines.map( function( line ) { line.draw(); } );

	rbM.restore();
}

rainbowMouse.step.spawn = function() {

	if( lines.length < opts.lineMaxCount && Math.random() < opts.lineSpawnProb )
		lines.push( new Line );
}
rainbowMouse.step.updateTick = function() {

	++tick;
}
rainbowMouse.draw.repaint = function() {

	rbM.globalCompositeOperation = 'destination-out';
	rbM.fillStyle = 'rgba(0,0,0,alp)'.replace( 'alp', opts.gridRepaintAlpha );
	rbM.fillRect( 0, 0, Wrainbox, Hrainbox );
	rbM.globalCompositeOperation = 'lighter';
}
rainbowMouse.draw.transform = function() {

	rbM.save();
	var scaleFactor = 1 + Math.sin( tick * opts.gridScalingInputMultiplier ) * opts.gridScalingMultiplier;

	rbM.translate( opts.gridCenterX, opts.gridCenterY );
	rbM.rotate( tick * opts.gridRotationVel );
	rbM.scale( scaleFactor, scaleFactor );

	rbM.lineWidth = .2;
}

function Line() {
	this.reset();
}
Line.prototype.reset = function() {
	this.head = new Vec( 0, 0 );
	this.path = [ this.head ];

	this.life = 0;
	this.hue = ( ( tick * opts.gridHueChange ) % 360 ) |0;
}

Line.prototype.step = function() {
	++this.life;

	this.step_increment();
	this.step_decrement();
}
Line.prototype.draw = function() {
	if( this.path.length === 0 ) return;
	var	x1 = this.path[ 0 ].x,
	y1 = this.path[ 0 ].y,
	x2, y2, dx, dy;

	for( var i = 1; i < this.path.length; ++i ) {

		rbM.strokeStyle = 'hsla(hue, 80%, 50%, alp)'
		.replace( 'hue', this.hue + ( Math.random() * opts.lineHueVariation ) |0 )
		.replace( 'alp', opts.lineAlpha / ( this.life / 80 ) );

		x2 = this.path[ i ].x;
		y2 = this.path[ i ].y;

		dx = ( x2 - x1 ) / opts.lineMidPoints;
		dy = ( y2 - y1 ) / opts.lineMidPoints;

		rbM.beginPath();
		rbM.moveTo( x1 * s + Math.random() * opts.lineMidJitter - opts.lineMidJitter / 2, y1 * s + Math.random() * opts.lineMidJitter - opts.lineMidJitter / 2 );

		for( var j = 1; j < opts.lineMidPoints - 1; ++j )
			rbM.lineTo(
				( x1 + dx * j ) * s + Math.random() * opts.lineMidJitter - opts.lineMidJitter / 2,
				( y1 + dy * j ) * s + Math.random() * opts.lineMidJitter - opts.lineMidJitter / 2
				);

		rbM.lineTo( x2 * s + Math.random() * opts.lineMidJitter - opts.lineMidJitter / 2, y2 * s + Math.random() * opts.lineMidJitter - opts.lineMidJitter / 2 );
		rbM.stroke();

		x1 = x2;
		y1 = y2;
	}
}
Line.prototype.step_increment = function() {
	if( Math.random() > opts.lineIncrementProb ) return;

	var vec,
	lastHead = this.path[ this.path.length - 2 ];

	do {

		vec = randomPos( this.head );
	} while ( lastHead && vec.x === lastHead.x && vec.y === lastHead.y );

	this.head = vec;
	this.path.push( vec );

	if( this.path.length >= opts.lineMaxLength ) this.path.shift();
}
Line.prototype.step_decrement = function() {
	if( this.life < opts.lineSafeTime || Math.random() > opts.lineDecrementProb ) return;

	this.path.length > 0 ?
	this.path.shift()
	:
	this.reset();
}

function Vec( x, y ) {
	this.x = x;
	this.y = y;
}

function randomPos( previous ) {
	var rad = radPart * ( ( Math.random() * opts.gridSideNum ) |0 );
	return new Vec( Math.cos( rad ) + previous.x, Math.sin( rad ) + previous.y );
}

rainbowMouse();